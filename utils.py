from math import log2
from random import sample
from node import NumericNode

def H(y):
    dct = dict()
    try:
        for data in y.transpose().values[0]:
            if data not in dct:
                dct[data] = 1
            else:
                dct[data] += 1
    except IndexError as identifier:
        pass

    total = 0
    for data_key, data_value in dct.items():
        total += data_value

    entropy = 0
    for data_key, data_value in dct.items():
        prob_value = data_value/total
        entropy += prob_value * log2(prob_value)
    return -entropy

def plurality_value(x, y):
    dct = dict()
    for data in y.transpose().values[0]:
        if data not in dct:
            dct[data] = 1
        else:
            dct[data] += 1
        
    v = list(dct.values())
    k = list(dct.keys())
    return k[v.index(max(v))]

def importance(x, y, classes):
    node_with_max_gain = None
    max_gain = float('-inf')
    for class_ in classes:
        node = NumericNode(class_)
        gain_value = node.get_gain(x, y)
        if (gain_value > max_gain):
            node_with_max_gain = node
            max_gain = gain_value
    return node_with_max_gain

def is_same_classification(x,y):
    list_class = list(y.values)
    return (len(list_class)==1), list_class[0]