#!/bin/bash

STATUS=$(curl -sLX POST -d "api-key=$HOST_API_KEY" $HOST_API_URL)

if [ "$STATUS" == "0" ]
then
    echo "SUCCESS"
    exit 0
elif [ "$STATUS" == "-1" ]
then
    echo "ERROR: Wrong API key"
    exit -1
elif [ "$STATUS" == "-2" ]
then
    echo "ERROR: git failed to pull"
    exit -2
elif [ "$STATUS" == "-3" ]
then
    echo "ERROR: container failed to start"
    exit -3
fi
