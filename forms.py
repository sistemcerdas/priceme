from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, SubmitField
from wtforms.fields.html5 import DecimalField, IntegerField

class PredictForm(FlaskForm):
    battery_power = DecimalField('Battery Power')
    blue = BooleanField('Bluetooth')
    clock_speed = DecimalField('Clock Speed')
    dual_sim = BooleanField('Dual SIM')
    fc = DecimalField('Front Camera Resolution')
    four_g = BooleanField('4G')
    int_memory = DecimalField('Internal Memory')
    m_dep = DecimalField('Mobile Depth')
    mobile_wt = DecimalField('Mobile Weight')
    n_cores = IntegerField('Number of Cores')
    pc = DecimalField('Primary Camera Resolution')
    px_height = DecimalField('Pixel Resolution Height')
    px_width = DecimalField('Pixel Resolution Width')
    ram = DecimalField('RAM')
    sc_h = DecimalField('Screen Height')
    sc_w = DecimalField('Screen Weight')
    talk_time = DecimalField('Talk Time')
    three_g = BooleanField('3G')
    touch_screen = BooleanField('Touch Screen')
    wifi = BooleanField('Wifi')
    submit = SubmitField('Predict')
