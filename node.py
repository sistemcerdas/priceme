from pandas import DataFrame
import utils

NODE_INDEX_LABEL = {
    'ram': 0,
    'battery_power': 1,
    "px_width": 2,
    "px_height": 3,
}

class NumericNode:
    def __init__(self, node_label):
        self.node_label = node_label
        self.left_node = None
        self.right_node = None
        self.split_point = None
        self.left_data_x = None
        self.left_data_y = None
        self.right_data_x = None
        self.right_data_y = None

    def predict(self, x):
        index = NODE_INDEX_LABEL[self.node_label]
        if (x[0][index] < self.split_point):
            if (isinstance(self.left_node, NumericNode)):
                return self.left_node.predict(x)
            return self.left_node
        else:
            if (isinstance(self.right_node, NumericNode)):
                return self.right_node.predict(x)
            return self.right_node

    def get_branch_data(self, x_data, y_data, split_point):
        left_branch_x = DataFrame({"ram": [], 
                   "battery_power": [], 
                   "px_width": [],
                   "px_height": []})
        left_branch_y = DataFrame()
        right_branch_x = DataFrame({"ram": [], 
                   "battery_power": [], 
                   "px_width": [],
                   "px_height": []})
        right_branch_y = DataFrame()
        for i in range(x_data.shape[0]):
            try:
                if (x_data[self.node_label][i] < split_point):
                    left_branch_x = left_branch_x.append(x_data.iloc[i])
                    left_branch_y = left_branch_y.append(DataFrame([y_data.values[i]]))
                else:
                    right_branch_x = right_branch_x.append(x_data.iloc[i])
                    right_branch_y = right_branch_y.append(DataFrame([y_data.values[i]]))
            except KeyError as e:
                pass
        return left_branch_x, left_branch_y, right_branch_x, right_branch_y

    def get_label(self):
        return self.node_label

    def get_left_data(self):
        return self.left_data_x, self.left_data_y

    def get_right_data(self):
        return self.right_data_x, self.right_data_y

    def assign_subtree(self, left_node=None, right_node=None):
        self.left_node = left_node
        self.right_node = right_node

    def get_gain(self, x, y):
        entropy = utils.H(y)
        split_point = x.median()[self.node_label]
        self.split_point = split_point
        left_branch_x, left_branch_y, right_branch_x, right_branch_y = self.get_branch_data(x, y, split_point)
        gain = entropy - (utils.H(left_branch_y) + utils.H(right_branch_y))

        self.left_data_x = left_branch_x
        self.left_data_y = left_branch_y
        self.right_data_x = right_branch_x
        self.right_data_y = right_branch_y
        return gain

    def __str__(self, level=0):
        ret = "\t"*level + self.node_label + "\n"

        if (isinstance(self.left_node, NumericNode)):
            ret += self.left_node.__str__(level+1)
        else:
            ret += "\t"*(level + 1) + str(self.left_node) + "\n"

        if (isinstance(self.right_node, NumericNode)):
            ret += self.right_node.__str__(level+1)
        else:
            ret += "\t"*(level + 1) + str(self.right_node) + "\n"

        return ret