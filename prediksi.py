from math import sqrt
from pandas import read_csv, DataFrame
from core import DecisionTreeClassifier
from sklearn.metrics import mean_squared_error, accuracy_score

PRICE_RANGE = [
    'Low Cost',
    'Medium Cost',
    'High Cost',
    'Very High Cost'
]

mobiles = read_csv("mobile.csv")

ram_values = mobiles['ram']

battery_power_values = mobiles['battery_power']
px_width_values = mobiles['px_width']
px_height_values = mobiles['px_height']
price_range_values = mobiles['price_range']

df = DataFrame({"ram": ram_values, 
                   "battery_power": battery_power_values, 
                   "px_width": px_width_values,
                   "px_height": px_height_values,
                   "price_range": price_range_values})

X = df.drop(['price_range'], axis = 1)
Y = DataFrame(df['price_range'])

dtl = DecisionTreeClassifier()
dtl.fit(X, Y)
print(dtl.print_tree())
def process(data):
    data = [[
        int(data.get("ram") or 0),
        int(data.get("battery_power") or 0),
        int(data.get("px_width") or 0),
        int(data.get("px_height") or 0),
    ]]
    
    output = dtl.predict(data)
    print(output)
    klasifikasi = output[0]

    return PRICE_RANGE[klasifikasi]

klasifikasi = process({
    "ram": 3000,
    "battery_power": 200,
    "px_width": 200,
    "px_height": 200
})

print(klasifikasi)