from math import log2
import pandas as pd
from pandas import DataFrame
from utils import (
    H,
    importance,
    plurality_value,
    is_same_classification
)

def DTL(x, y, classes, parent_examples_x, parent_examples_y):
    if (x.shape[0] == 0):
        return plurality_value(parent_examples_x, parent_examples_y)
    is_same, classification = is_same_classification(x,y)
    if (is_same):
        return classification
    if (len(classes) == 0):
        return plurality_value(x,y)
    highest_gain_node = importance(x,y, classes)
    left_data_x, left_data_y = highest_gain_node.get_left_data()
    right_data_x, right_data_y = highest_gain_node.get_right_data()
    subtree_left = DTL(left_data_x, left_data_y, classes - {highest_gain_node.get_label()}, x,y)
    subtree_right = DTL(right_data_x, right_data_y, classes - {highest_gain_node.get_label()}, x,y)

    highest_gain_node.assign_subtree(subtree_left, subtree_right)

    return highest_gain_node

class DecisionTreeClassifier:
    def __init__(self):
        self.trained = False
        self.root = None

    def fit(self, x, y):
        self.trained = True
        self.root = DTL(x, y, {'ram', 'battery_power', 'px_width', 'px_height'}, DataFrame({"ram": [], 
                   "battery_power": [], 
                   "px_width": [],
                   "px_height": []}),
                   DataFrame()
        )

    def predict(self, x):
        return self.root.predict(x)

    def print_tree(self):
        return str(self.root)