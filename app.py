from flask import Flask, render_template, request
from forms import PredictForm
import prediksi
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)

@app.route("/")
def hello():
    context = {'index_active': 'btn-primary'}
    return render_template('index.html', **context)

@app.route('/predict/', methods=['GET', 'POST'])
def form():
    context = {'form_active': 'btn-primary'}
    form = PredictForm()

    if request.method == 'POST':
        data = request.form.to_dict()
        context['result'] = prediksi.process(data)
        return render_template('result.html', **context)
    else:
        context['form'] = form
        return render_template('form.html', **context)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
